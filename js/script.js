$(document).ready(function () {
  //3 paragraphs
  var para1 = {
    paragraph:
      "Since our Draft Capstone Write-Up a few weeks ago, we have progressed further into our class, and further into JavaScript. The new content we have been working on (AJAX, jQuery, etc), while difficult, have been remarkably interesting to work with. In the Draft Write-Up, I initially said that I did not know who I am as a designer, and while I still do not fully know yet, going through the functionality of JavaScript, I have found an appreciation for knowing/figuring out how things work in JavaScript. However, I still want to find a certain style that I enjoy, and I feel is easy for people visiting my site to traverse. Still, the most important thing that I want people to take away from viewing my work is I want them to see my improvement throughout the class, especially when we get deeper into the more difficult corners of JavaScript, because while I am not amazing at doing it, I enjoy working it out and trying to figure out how to make it work. I want the audience to see the time and thought I have put into my work where I can effectively show it."
  };
  var para2 = {
    paragraph:
      "I feel that I have become stronger in these classes since the Draft Capstone Write-Up, especially in HTML and CSS. Compared to the inner workings of JavaScript – while it is interesting to work with – I am objectively better at working with HTML and CSS than I am with JS. At this point, I am familiar with HTML and CSS while we have only been working on JS for a short amount of time compared to the other two. Another skill that I have is my willingness to learn, especially learning things like coding where you are working behind the surface level of what people normally see. My interest in coding paired with my willingness to learn is what allows me to get lost in coding, and I sometimes end up fiddling with it all day. "
  };
  var para3 = {
    paragraph:
      "One of the biggest things that I am using for inspiration in this class is of course, the walkthrough of the labs that we do every lecture. Personally, I am a visual/work-through-it learner so when I have someone to lead me through exactly what we are supposed to do accompanied by explanations of what is happening with each line of code, it inspires me to try new things that use the same components we learn. Whenever I do not know how to do something for a lab, I initially go to w3schools or stack overflow for help. Another slight inspiration for my coding comes from a tech-youtuber named Michael Reeves. He sometimes streams himself working on projects through vs code, and funny enough, I have used his content to help myself with one of our labs, as well as understand more about JS in general."
  };

  //array of panels
  var paras = new Array();
  paras.push(para1);
  paras.push(para2);
  paras.push(para3);

  // apply bootstrap panel classes
  $("ol").addClass("list-group");
  $("li").addClass("list-group-item");

  $("li").each(function (i) {
    let str = paras[i].paragraph;
    $(this).html(str);
  });

  //stiped panels
  $("li").each(function (i) {
    if (i % 2 != 0) {
      $(this).addClass("even");
    } else {
      $(this).addClass("odd");
    }
  });
});